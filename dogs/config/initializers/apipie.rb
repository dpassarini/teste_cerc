Apipie.configure do |config|
  config.app_name                = "Dogs"
  config.api_base_url            = "/v1"
  config.doc_base_url            = "/apipie"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/v1/**.rb"
  config.app_info = "API para consulta de cachorros"
  config.translate = false
  config.validate = false
end
