require 'sidekiq/web'
Rails.application.routes.draw do
  apipie
  mount Sidekiq::Web => '/sidekiq'
  namespace :v1 do
    resources :breeds, param: :identifier, only: [:show]
    resources :batches, only: [:create, :show]
  end
end
