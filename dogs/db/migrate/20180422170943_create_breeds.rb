class CreateBreeds < ActiveRecord::Migration[5.2]
  def change
    create_table :breeds, id: :uuid do |t|
      t.string :name
      t.string :identifier
      t.integer :max_life_expectation
      t.integer :min_life_expectation
      t.string :country

      t.timestamps
    end
  end
end
