class CreateBatchItems < ActiveRecord::Migration[5.2]
  def change
    create_table :batch_items, id: :uuid do |t|
      t.references :batch, type: :uuid, foreign_key: true
      t.references :breed, type: :uuid, foreign_key: true
      t.string :status, default: 'pendente'

      t.timestamps
    end
  end
end
