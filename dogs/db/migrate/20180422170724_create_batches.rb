class CreateBatches < ActiveRecord::Migration[5.2]
  def change
    create_table :batches, id: :uuid do |t|
      t.string :status, default: 'pendente'

      t.timestamps
    end
  end
end
