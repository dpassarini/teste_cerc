require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe V1::BatchesController, type: :controller do

  describe "POST #create" do
    before do
      Sidekiq::Worker.clear_all
    end
    it "returns the job created id and enque the job" do
      Sidekiq::Testing.fake!
      post :create, params: { racas: ['dachshund'] }
      expect(ProcessBatchWorker.jobs.size).to eq(1)
      expect(response.status).to eq(200)
    end

    it "should returns an error and do not create a job" do
      Sidekiq::Testing.fake!
      post :create, params: {}
      expect(ProcessBatchWorker.jobs.size).to eq(0)
      expect(response.status).to eq(400)
      expect(Batch.count).to eq(0)
    end
  end


  describe "GET #show" do
    it "returns a pending batch" do
      batch = Batch.create
      get :show, params: {id: batch.id}
      parsed = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(parsed['status']).to eq('pendente')
    end

    it 'should return a full processed batch' do
      batch = Batch.create!
      identifiers = ['chow-chow', 'golden-retriever', 'dachshund']
      Sidekiq::Testing.inline! do
        ProcessBatchWorker.perform_async(batch.id, identifiers)
      end
      get :show, params: {id: batch.id}
      parsed = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(parsed['racas'][0]['identificador']).to eq('chow-chow')
      expect(parsed['racas'][1]['identificador']).to eq('golden-retriever')
      expect(parsed['racas'][2]['identificador']).to eq('dachshund')
      expect(parsed['status']).to eq('concluido')
    end
  end

end
