require 'rails_helper'

RSpec.describe V1::BreedsController, type: :controller do
  describe 'GET #show' do
    let(:mocked_response) do
      {
        response: {
          name: 'Golden Retriever',
          identifier: 'golden-retriever',
          max_life_expectation: 12,
          min_life_expectation: 11,
          country: 'Reino Unido'
        },
        status: 200
      }
    end

    before do
      breed = {
        name: 'Dachshund',
        identifier: 'dachshund',
        max_life_expectation: 15,
        min_life_expectation: 10,
        country: 'Alemanha'
      }
      Breed.create(breed)
    end

    it 'returns existing breed' do
      get :show, params: { identifier: 'dachshund' }
      parsed_resp = JSON.parse(response.body)
      expect(parsed_resp['identifier']).to eq('dachshund')
      expect(parsed_resp['country']).to eq('Alemanha')
      expect(response.status).to eq(200)
    end

    it 'returns a consulted breed' do
      allow_any_instance_of(BreedConsultingService).to receive(:get_payload).and_return(mocked_response)
      get :show, params: { identifier: 'golden-retriever' }

      parsed_resp = JSON.parse(response.body)
      expect(parsed_resp['identifier']).to eq('golden-retriever')
      expect(parsed_resp['country']).to eq('Reino Unido')
      expect(parsed_resp['min_life_expectation']).to eq(11)
      expect(response.status).to eq(200)
    end
  end
end
