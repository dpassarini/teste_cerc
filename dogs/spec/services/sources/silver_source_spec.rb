require 'rails_helper'

RSpec.describe Sources::SilverSource do
  let(:mocked_response) {
    OpenStruct.new({
      body: {
        nome_da_raca: "Golden Retriever",
        expectativa_de_vida_minima: 10,
        expectativa_de_vida_maxima: 12,
        pais_de_origem: "Reino Unido"
      }.to_json,
      status: 200
    })
  }

  let(:expected_response) {
    {
      name: 'Golden Retriever',
      identifier: 'golden-retriever',
      max_life_expectation: 12,
      min_life_expectation: 10,
      country: 'Reino Unido'
    }
  }

  describe '#processed_payload' do
    it 'should return the formatted response' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(mocked_response)
      payload = Sources::SilverSource.new(@breed_identifier).processed_payload
      expect(payload).to eq(expected_response)
    end
  end
end
