require 'rails_helper'

RSpec.describe Sources::PlatinumSource do
  let(:mocked_response) {
    OpenStruct.new({
      body: {
        breed: {
          name: "Golden Retriever",
          life_span: {
            min: 10,
            max: 12
          },
          country_of_origin: "United Kingdom"
        }
      }.to_json,
      status: 200
    })
  }

  let(:expected_response) {
    {
      name: 'Golden Retriever',
      identifier: 'golden-retriever',
      max_life_expectation: 12,
      min_life_expectation: 10,
      country: 'Reino Unido'
    }
  }

  describe '#processed_payload' do
    it 'should return the formatted response' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(mocked_response)
      payload = Sources::PlatinumSource.new(@breed_identifier).processed_payload
      expect(payload).to eq(expected_response)
    end
  end
end
