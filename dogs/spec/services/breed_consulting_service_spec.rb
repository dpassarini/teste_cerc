require 'rails_helper'

RSpec.describe BreedConsultingService do
  describe '#get_payload' do
    let(:platinum_response) {
      OpenStruct.new({
        body: {
          breed: {
            name: "Golden Retriever",
            life_span: {
              min: 10,
              max: 12
            },
            country_of_origin: "United Kingdom"
          }
        }.to_json,
        status: 200
      })
    }

    let(:silver_response) {
      OpenStruct.new({
        body: {
          nome_da_raca: "Golden Retriever",
          expectativa_de_vida_minima: 10,
          expectativa_de_vida_maxima: 12,
          pais_de_origem: "Reino Unido"
        }.to_json,
        status: 200
      })
    }

    let(:error_response) {
      OpenStruct.new({
        body: {}.to_json,
        status: 500
      })
    }


    it 'should get data from silver source' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(silver_response)
      bcs = BreedConsultingService.new('golden-retriever')
      payload = bcs.get_payload
      expect(payload[:source]).to eq('silver')
    end

    it 'should get data from platinum source' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(error_response)
      bcs = BreedConsultingService.new('golden-retriever')
      payload = bcs.get_payload
      expect(payload[:source]).to eq('platinum')
    end
  end

  describe '#consult_breed' do
    let(:silver_response) {
      OpenStruct.new({
        body: {
          nome_da_raca: "Golden Retriever",
          expectativa_de_vida_minima: 10,
          expectativa_de_vida_maxima: 12,
          pais_de_origem: "Reino Unido"
        }.to_json,
        status: 200
      })
    }

    it 'should return a valid breed' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(silver_response)
      bcs = BreedConsultingService.new('golden-retriever')
      valid_breed = bcs.consult_breed

      expect(valid_breed.class).to eq(Breed)
      expect(valid_breed.name).to eq('Golden Retriever')
      expect(valid_breed.id).to be_truthy
    end

    it 'should return a valid breed but with no data' do
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(silver_response)
      bcs = BreedConsultingService.new('dachshund')
      valid_breed = bcs.consult_breed
      
      expect(valid_breed.class).to eq(Breed)
      expect(valid_breed.name).to eq('Dados não encontrados')
      expect(valid_breed.id).to be_truthy
    end
  end
end
