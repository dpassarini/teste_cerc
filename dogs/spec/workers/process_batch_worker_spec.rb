require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe ProcessBatchWorker, type: :worker do
  context 'When try to add a new batch' do
    let(:mocked_response) do
      {
        response: {
          name: 'Dachshund',
          identifier: 'dachshund',
          max_life_expectation: 15,
          min_life_expectation: 11,
          country: 'Alemanha'
        },
        status: 200
      }
    end
    before do
      Sidekiq::Worker.clear_all
    end

    it 'should process the batch' do
      allow_any_instance_of(BreedConsultingService).to receive(:get_payload).and_return(mocked_response)
      batch = Batch.create!
      identifiers = ['chow-chow', 'golden-retriever', 'dachshund']
      Sidekiq::Testing.inline! do
        ProcessBatchWorker.perform_async(batch.id, identifiers)
      end
      batch.reload

      expect(batch.batch_items[0].breed.identifier).to eq('chow-chow')
      expect(batch.batch_items[1].breed.identifier).to eq('golden-retriever')
      expect(batch.batch_items[2].breed.identifier).to eq('dachshund')
      expect(batch.status).to eq('concluido')
    end
  end
end
