require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe ProcessBatchItemWorker, type: :worker do
  context 'When try to add a new item to batch' do
    let(:mocked_response) do
      {
        response: {
          name: 'Dachshund',
          identifier: 'dachshund',
          max_life_expectation: 15,
          min_life_expectation: 11,
          country: 'Alemanha'
        },
        status: 200
      }
    end

    before do
      Sidekiq::Worker.clear_all
    end

    it 'should add a new item' do
      allow_any_instance_of(BreedConsultingService).to receive(:get_payload).and_return(mocked_response)
      batch = Batch.create!
      batch_item = batch.batch_items.create!
      identifier = 'dachshund'
      Sidekiq::Testing.inline! do
        ProcessBatchItemWorker.perform_async(batch_item.id, identifier)
      end
      batch_item.reload
      batch.reload
      expect(batch.status).to eq('concluido')
      expect(batch_item.breed.identifier).to eq('dachshund')
    end
  end
end
