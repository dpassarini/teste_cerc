require 'rails_helper'

RSpec.describe Batch, type: :model do
  describe '#processed?' do
    let(:batch) { Batch.create }

    it 'should return true for a concluded batch' do
      batch.update_attributes(status: 'concluido')
      expect(batch.processed?).to be_truthy
    end

    it 'should return false for a pending with no batch item' do
      expect(batch.processed?).to be_falsey
    end

    it 'should return false for a batch with unprocessed batch_items' do
      batch.batch_items << BatchItem.create(status: 'pendente')
      batch.batch_items << BatchItem.create(status: 'concluido')

      expect(batch.processed?).to be_falsey
    end

    it 'should return true for a batch only processed batch_items' do
      batch.batch_items << BatchItem.create(status: 'concluido')
      batch.batch_items << BatchItem.create(status: 'concluido')

      expect(batch.processed?).to be_truthy
    end
  end

  describe '#batch_items_response_formatted' do
    let(:batch) { Batch.create }
    let(:dachshund) {
      {
        name: 'Dachshund',
        identifier: 'dachshund',
        max_life_expectation: 15,
        min_life_expectation: 10,
        country: 'Alemanha',
      }
    }
    let(:golden) {
      {
        name: 'Golden Retriever',
        identifier: 'golden-retriever',
        max_life_expectation: 12,
        min_life_expectation: 11,
        country: 'Reino Unido',
      }
    }

    it 'should return a list of formatted items' do
      expected_response = [
        {
          :identificador=>"dachshund",
          :nome=>"Dachshund",
          :expectativa_de_vida=>{:minima=>10, :maxima=>15},
          :pais=>"Alemanha"
        },
        {
          :identificador=>"golden-retriever",
          :nome=>"Golden Retriever",
          :expectativa_de_vida=>{:minima=>11, :maxima=>12},
          :pais=>"Reino Unido"
        }
      ]
      batch.batch_items << BatchItem.create(status: 'concluido', breed: Breed.create(dachshund))
      batch.batch_items << BatchItem.create(status: 'concluido', breed: Breed.create(golden))
      expect(batch.batch_items_response_formatted).to eq(expected_response)
    end
  end
end
