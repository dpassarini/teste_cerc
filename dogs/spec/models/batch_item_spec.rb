require 'rails_helper'

RSpec.describe BatchItem, type: :model do
  describe '#item_response_formatted' do
    let(:batch) { Batch.create }
    let(:dachshund) {
      Breed.create({
        name: 'Dachshund',
        identifier: 'dachshund',
        max_life_expectation: 15,
        min_life_expectation: 10,
        country: 'Alemanha',
      })
    }
    let(:golden) {
      Breed.create({
        name: 'Golden Retriever',
        identifier: 'golden-retriever',
        max_life_expectation: 12,
        min_life_expectation: 11,
        country: 'Reino Unido',
      })
    }
    it 'should return nil for an item without breed' do
      batch_item = BatchItem.create(status: 'concluido', batch: batch)
      expect(batch_item.item_response_formatted).to be_nil
    end

    it 'should return the formatted item' do
      expected_response = {
        :identificador=>"golden-retriever",
        :nome=>"Golden Retriever",
        :expectativa_de_vida=>{:minima=>11, :maxima=>12},
        :pais=>"Reino Unido"
      }
      batch_item = BatchItem.create(status: 'concluido', batch: batch, breed: golden)
      expect(batch_item.item_response_formatted).to eq(expected_response)
    end
  end
end
