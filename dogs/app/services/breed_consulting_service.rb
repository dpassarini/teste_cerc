class BreedConsultingService
  attr_reader :breed

  def initialize(breed_identifier)
    @breed_identifier = breed_identifier
  end

  def get_payload
    source = 'silver'
    payload = Sources::SilverSource.new(@breed_identifier).processed_payload
    if payload.nil?
      source = 'platinum'
      payload = Sources::PlatinumSource.new(@breed_identifier).processed_payload
    end
    { response: payload, status: payload.nil? ? 500 : 200, source: source }
  end

  def consult_breed
    breed = Breed.where(identifier: @breed_identifier).first
    return breed unless breed.nil?
    breed_response = get_payload

    if breed_response[:status] == 200 && breed_response[:response].present? && breed_response[:response][:identifier] == @breed_identifier
      breed = Breed.create(breed_response[:response])
    else
      breed = Breed.create(identifier: @breed_identifier, name: 'Dados não encontrados')
    end
    breed
  end
end
