module Sources
  class SilverSource < BaseSource
    URL = 'https://private-anon-f231191e3e-cerctestedevdocumentacaodasfontes.apiary-mock.com/caes'.freeze

    def processed_payload
      response = get_response
      if response.status == 200
        json = JSON.parse(response.body)
        return {
          name: json['nome_da_raca'],
          identifier: json['nome_da_raca'].parameterize,
          max_life_expectation: json['expectativa_de_vida_maxima'],
          min_life_expectation: json['expectativa_de_vida_minima'],
          country: json['pais_de_origem']
        }
      end
      nil
    end

    private

    def url
      URL
    end
  end
end
