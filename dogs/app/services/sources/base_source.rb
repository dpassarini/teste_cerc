module Sources
  class BaseSource
    def initialize(breed)
      @url = "#{url}/#{breed}"
    end

    def get_response
      Faraday.get @url
    end
  end
end
