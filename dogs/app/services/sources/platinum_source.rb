module Sources
  class PlatinumSource < BaseSource
    URL = 'https://private-anon-f231191e3e-cerctestedevdocumentacaodasfontes.apiary-mock.com/dogs/'.freeze

    def processed_payload
      response = get_response
      if response.status == 200
        json = JSON.parse(response.body)
        return {
          name: json['breed']['name'],
          identifier: json['breed']['name'].parameterize,
          max_life_expectation: json['breed']['life_span']['max'],
          min_life_expectation: json['breed']['life_span']['min'],
          country: ISO3166::Country.find_country_by_name(json['breed']['country_of_origin']).translated_names.first
        }
      end
      nil
    end

    private

    def url
      URL
    end
  end
end
