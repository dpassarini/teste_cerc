class V1::BreedsController < ApplicationController
  resource_description do
    short 'Dogs API'
    formats ['json']
    error 500, "Default error status"
  end

  api :GET, '/breeds/:identifier'
  param :identifier, String, :desc => 'Identificador da raça a qual queria consultar', required: true
  description 'Metodo para consulta avulsa de raça'
  def show
    bcs = BreedConsultingService.new(params[:identifier])
    breed = bcs.consult_breed
    breed_response, status = { response: 'Breed not found' }, :not_found
    if breed.present?
       breed_response, status = breed.attributes.except('id', 'created_at', 'updated_at'), :ok
     end
    render json: breed_response, status: :ok
  end
end
