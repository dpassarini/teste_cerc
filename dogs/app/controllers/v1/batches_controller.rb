class V1::BatchesController < ApplicationController
  resource_description do
    short 'Dogs API'
    formats ['json']
    error 500, "Default error status"
  end

  api :POST, '/batches'
  description 'Metodo para criar um lote de consulta de raças'
  def create
    begin
      if params[:racas].present? # && batch_params[:racas].kind_of?(Array)
        batch = Batch.create!
        ProcessBatchWorker.perform_async(batch.id, params[:racas])
        render json: { id: batch.id, status: 'pendente' }, status: 200
      else
        render json: { erro: 'informe as raças corretamente, por favor' }, status: 400
      end
    rescue
      render json: {}, status: 500
    end
  end

  api :GET, '/batches/:id'
  param :id, String, :desc => 'Identificador do lote que será consultado', required: true
  description 'Metodo para consultar o estado do lote de raças'
  def show
    batch = Batch.find(params[:id])
    response = {
      id: batch.id,
      status: batch.status
    }
    response[:racas] = batch.batch_items_response_formatted if batch.processed?
    render json: response, status: 200
  end

  private
  def batch_params
    params.permit(:racas)
  end
end
