class Batch < ApplicationRecord
  has_many :batch_items

  def processed?
    return true if status == 'concluido'
    return false if batch_items.count == 0
    batch_items.each do |item|
      return false unless item.status == 'concluido'
    end
    true
  end

  def batch_items_response_formatted
    response = []
    batch_items.each do |item|
      response << item.item_response_formatted
    end
    response
  end
end
