class Breed < ApplicationRecord
  validates :identifier, uniqueness: true
end
