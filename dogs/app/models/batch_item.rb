class BatchItem < ApplicationRecord
  belongs_to :batch, optional: true
  belongs_to :breed, optional: true

  def item_response_formatted
    return nil if breed_id.nil?
    {
      identificador: breed.identifier,
      nome: breed.name,
      expectativa_de_vida: {
        minima: breed.min_life_expectation,
        maxima: breed.max_life_expectation
      },
      pais: breed.country
    }
  end
end
