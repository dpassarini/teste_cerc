class ProcessBatchItemWorker
  include Sidekiq::Worker

  def perform(item_id, breed_identifier)
    item = BatchItem.find(item_id)
    bcs = BreedConsultingService.new(breed_identifier)
    breed = bcs.consult_breed
    
    if breed.nil?
      item.update_attributes(status: 'concluido')
    else
      item.update_attributes(breed_id: breed.id, status: 'concluido')
    end
    item.batch.update_attributes(status: 'concluido') if item.batch.processed?
  end
end
