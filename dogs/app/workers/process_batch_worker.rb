class ProcessBatchWorker
  include Sidekiq::Worker

  def perform(batch_id, breeds)
    batch = Batch.find(batch_id)
    batch.update_attributes(status: 'processando')
    breeds.each do |breed|
      item = BatchItem.create!(batch: batch)
      ProcessBatchItemWorker.perform_async(item.id, breed)
    end
  end
end
